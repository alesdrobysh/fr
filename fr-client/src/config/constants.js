export const FACE_NOT_RECOGNIZED_RESPONSE = 'Face is not recognized';
export const FACE_RECOGNIZED_RESPONSE = 'Face is recognized';

export const nameIsRegistered = (name) => `${name} is registered`;
export const CAPTURE = 'Capture';
export const TRY_AGAIN = 'Try Again';
export const REGISTER_FACE = 'Register Face';
export const PLEASE_INPUT_YOUR_NAME = 'Please input your name!';
export const NAME = 'Name';

export const FAILED_FETCHING_SERIAL_PORTS = 'Failed fetching serial ports';
export const SERIAL_PORTS = 'Serial Ports';
export const LOG_OUT = 'Log out';
