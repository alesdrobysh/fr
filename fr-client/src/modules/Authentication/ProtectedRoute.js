import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { withAuthentication } from './AuthenticationProvider';

function PrivateRoute(
  {
    component: Component,
    authentication,
    ...rest
  }) {
  return (
    <Route
      {...rest}
      render={(props) =>
        authentication.name ? (
          <Component {...props} />
        ) : (
            <Redirect
              to={{
                pathname: "/authenticate",
                state: { from: props.location }
              }}
            />
          )
      }
    />
  );
}

export default withAuthentication(PrivateRoute);
