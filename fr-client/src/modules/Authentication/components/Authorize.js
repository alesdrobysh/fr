import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import Video from './Video';
import RegisterFaceForm from './RegisterFaceForm';

import {
  withAuthentication,
  withAuthenticationPropTypes,
} from '..';

import { compose } from '../../util';

const styles = {
  result: {
    width: '640px',
    margin: '8px auto',
    textAlign: 'center',
  },
}

class Authorize extends Component {
  static propTypes = {
    ...withAuthenticationPropTypes,
    classes: PropTypes.objectOf(PropTypes.string).isRequired,
  };

  state = {
    result: '',
    unrecognizedFaceEncoding: [],
    modalVisible: false,
    confirmLoading: false,
  };

  openRegisterModal = (unrecognizedFaceEncoding) => this.setState({
    unrecognizedFaceEncoding,
    modalVisible: true,
  });

  closeRegisterModal = () => this.setState({
    unrecognizedFaceEncoding: [],
    modalVisible: false,
  });

  onVideoResult = (result) => this.setState({ result });

  render() {
    const { classes } = this.props;
    const {
      modalVisible,
      confirmLoading,
    } = this.state;

    if (this.props.authentication.name) {
      return (
        <Redirect to="/" />
      );
    }

    return (
      <React.Fragment>
        <Video
          onRegisterClick={this.openRegisterModal}
          onResult={this.onVideoResult}
        />

        <div className={classes.result}>
          {this.state.result}
        </div>


        <RegisterFaceForm
          modalVisible={modalVisible}
          confirmLoading={confirmLoading}
          closeModal={this.closeRegisterModal}
          unrecognizedFaceEncoding={this.state.unrecognizedFaceEncoding}
        />
      </React.Fragment>
    );
  }
}

export default compose(
  injectSheet(styles),
  withAuthentication,
)(Authorize);
