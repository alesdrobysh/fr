import React, { Component } from 'react';
import injectSheet from 'react-jss';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import VideoControls from './VideoControls';

import { constants } from '../../../config';
import { api, notification } from '../../../services';
import {
  withAuthentication,
  withAuthenticationPropTypes,
} from '../AuthenticationProvider';
import { compose } from '../../util';

const styles = {
  image: {
    width: '640px',
    height: '480px',
    margin: '8px auto',
  },
  video: {
    height: '100%',
    width: '100%',
  },
  canvas: {
    height: '100%',
    width: '100%',
  },
  hidden: {
    display: 'none',
  },
};

class Video extends Component {
  static propTypes = {
    ...withAuthenticationPropTypes,
    classes: PropTypes.objectOf(PropTypes.string).isRequired,
    onRegisterClick: PropTypes.func.isRequired,
    onResult: PropTypes.func.isRequired,
  };

  state = {
    result: '',
    unrecognizedFaceEncoding: [],
    videoHidden: false,
    canvasHidden: true,
    captureHidden: false,
    againHidden: true,
    registerHidden: true,
    spinHidden: true,
    confirmLoading: false,
  };

  async componentDidMount() {
    try {
      this.video.srcObject = await navigator.mediaDevices.getUserMedia({ video: true });
    } catch (e) {
      console.error(e);
    }
  }

  setVideoRef = (element) => {
    this.video = element;
  };

  setCanvasRef = (element) => {
    this.canvas = element;
  };

  buildVideoClassNames = () => classNames({
    [this.props.classes.video]: true,
    [this.props.classes.hidden]: this.state.videoHidden,
  });

  buildCanvasClassNames = () => classNames({
    [this.props.classes.canvas]: true,
    [this.props.classes.hidden]: this.state.canvasHidden,
  });

  processRecognitionResponse = (response) => {
    const faceNotRecognized = response.data.message === constants.FACE_NOT_RECOGNIZED_RESPONSE;
    const faceRecognized = response.data.message === constants.FACE_RECOGNIZED_RESPONSE;

    const registerHidden = !faceNotRecognized;
    const result = faceRecognized ? undefined : response.data.message;
    const unrecognizedFaceEncoding = faceNotRecognized ? response.data.encoding : [];

    if (faceRecognized) {
      this.props.authentication.setName(response.data.name);
      return;
    }

    return { registerHidden, result, unrecognizedFaceEncoding };
  }

  handleCaptureClick = () => {
    this.setState({
      videoHidden: true,
      canvasHidden: false,
      captureHidden: true,
      spinHidden: false,
    });

    // Draw the video frame to the canvas.
    const context = this.canvas.getContext('2d');
    context.drawImage(this.video, 0, 0, this.canvas.width, this.canvas.height);

    this.canvas.toBlob(async (blob) => {
      const formData = new FormData();
      formData.append('file', blob, 'selfie.jpg');

      try {
        const response = await api.recognize(formData)
        const prosessedResponse = this.processRecognitionResponse(response);

        if (prosessedResponse) {
          this.props.onResult(prosessedResponse.result);
          this.setState({
            ...prosessedResponse,
            spinHidden: true,
            againHidden: false,
          });
        }
      } catch (e) {
        console.error(e);
        notification.error(e.message);

        this.setState({
          spinHidden: true,
          againHidden: false,
        });
      }
    });
  };

  handleTryAgainClick = () => {
    this.setState({
      result: '',
      videoHidden: false,
      canvasHidden: true,
      captureHidden: false,
      againHidden: true,
      registerHidden: true,
      unrecognizedFaceEncoding: [],
    });
  };

  handleRegisterClick = () => {
    this.props.onRegisterClick(this.state.unrecognizedFaceEncoding);
  };

  render() {
    const { classes } = this.props;
    const {
      captureHidden,
      againHidden,
      registerHidden,
      spinHidden
    } = this.state;

    return (
      <React.Fragment>
        <div className={classes.image}>
          <video
            className={this.buildVideoClassNames()}
            ref={this.setVideoRef}
            autoPlay
          />
          <canvas
            className={this.buildCanvasClassNames()}
            ref={this.setCanvasRef}
          />
        </div>

        <VideoControls
          handleCaptureClick={this.handleCaptureClick}
          handleTryAgainClick={this.handleTryAgainClick}
          handleRegisterClick={this.handleRegisterClick}
          captureHidden={captureHidden}
          againHidden={againHidden}
          registerHidden={registerHidden}
          spinHidden={spinHidden}
        />
      </React.Fragment>
    );
  }
};

export default compose(
  injectSheet(styles),
  withAuthentication,
)(Video);