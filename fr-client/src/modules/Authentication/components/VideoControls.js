import React from 'react';
import { Button, Spin } from 'antd';
import classNames from 'classnames';
import injectSheet from 'react-jss';

import { constants } from '../../../config';

const styles = {
  actionButton: {
    margin: '0 4px',
  },
  hidden: {
    display: 'none',
  },
  actions: {
    width: '640px',
    margin: '8px auto',
    textAlign: 'center',
  },
}

const VideoControls = (
  {
    classes,
    handleCaptureClick,
    handleTryAgainClick,
    handleRegisterClick,
    captureHidden,
    againHidden,
    registerHidden,
    spinHidden,
  }
) => {
  const buildCaptureButtonClassNames = () => classNames({
    [classes.actionButton]: true,
    [classes.hidden]: captureHidden,
  });

  const buildTryAgainButtonClassNames = () => classNames({
    [classes.actionButton]: true,
    [classes.hidden]: againHidden,
  });

  const buildRegisterButtonClassNames = () => classNames({
    [classes.actionButton]: true,
    [classes.hidden]: registerHidden,
  });

  const buildSpinClassNames = () => classNames({
    [classes.hidden]: spinHidden,
  });

  return (
    <div className={classes.actions}>
      <Button
        type="primary"
        icon="search"
        className={buildCaptureButtonClassNames()}
        onClick={handleCaptureClick}
      >
        {constants.CAPTURE}
      </Button>
      <Button
        type="primary"
        icon="reload"
        className={buildTryAgainButtonClassNames()}
        onClick={handleTryAgainClick}
      >
        {constants.TRY_AGAIN}
      </Button>
      <Button
        type="primary"
        icon="cloud-upload-o"
        className={buildRegisterButtonClassNames()}
        onClick={handleRegisterClick}
      >
        {constants.REGISTER_FACE}
      </Button>
      <Spin className={buildSpinClassNames()} />
    </div>
  );
};

export default injectSheet(styles)(VideoControls);
