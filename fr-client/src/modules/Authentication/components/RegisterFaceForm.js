import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Modal } from 'antd';

import { constants } from '../../../config';
import { api, notification } from '../../../services';
import { compose } from '../../util';
import { withAuthentication, withAuthenticationPropTypes } from '../AuthenticationProvider';

const FormItem = Form.Item;

class RegisterFaceForm extends Component {
  static propTypes = {
    modalVisible: PropTypes.bool.isRequired,
    confirmLoading: PropTypes.bool.isRequired,
    closeModal: PropTypes.func.isRequired,
    unrecognizedFaceEncoding: PropTypes.arrayOf(PropTypes.number).isRequired,
    ...withAuthenticationPropTypes,
  };

  setFormRef = (element) => {
    this.form = element;
  };

  handleRegisterOk = (event, values) => {
    this.props.form.validateFields(async (err, values) => {
      if (err) {
        return;
      }

      this.setState({
        confirmLoading: true,
      });

      const { name } = values;
      const data = {
        name,
        encoding: this.props.unrecognizedFaceEncoding,
      };

      try {
        await api.register(data);

        notification.success(constants.nameIsRegistered(name));

        this.props.authentication.setName(name);
      } catch (e) {
        console.error(e.response);
        notification.error(e.message);

        this.props.closeModal();
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { modalVisible, confirmLoading, closeModal } = this.props;
    return (
      <Modal title={constants.REGISTER_FACE}
        visible={modalVisible}
        onOk={this.handleRegisterOk}
        confirmLoading={confirmLoading}
        onCancel={closeModal}
      >
        <Form>
          <FormItem>
            {getFieldDecorator('name', {
              rules: [{ required: true, message: constants.PLEASE_INPUT_YOUR_NAME }],
            })(
              <Input placeholder={constants.NAME} />
            )}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default compose(
  Form.create(),
  withAuthentication,
)(RegisterFaceForm);
