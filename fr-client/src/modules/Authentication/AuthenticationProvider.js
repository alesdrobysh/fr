import React from 'react';
import PropTypes from 'prop-types';

const AuthenticationContext = React.createContext();

export class AuthenticationProvider extends React.Component {
  constructor(props) {
    super(props);

    const localStorageName = localStorage.getItem('name');
    const localStorageNameIsDefined = localStorageName && localStorageName !== 'undefined';

    const name = localStorageNameIsDefined ? localStorageName : undefined;

    this.state = {
      name,
    };
  }

  setName = (name) => {
    if (name) {
      console.log(`${name} logged in`);
    }
    this.setState({ name });
    localStorage.setItem('name', name);
  }

  render() {
    const providerValue = {
      name: this.state.name,
      setName: this.setName,
    };

    return (
      <AuthenticationContext.Provider value={providerValue}>
        {this.props.children}
      </AuthenticationContext.Provider>
    );
  }
}

export const withAuthentication = (Component) => {
  function WithAuthenticationHoc(props) {
    return (
      <AuthenticationContext.Consumer>
        {(context) => <Component {...props} authentication={context} />}
      </AuthenticationContext.Consumer>
    );
  }

  WithAuthenticationHoc.displayName = `WithAuthentication(${Component.displayName || Component.name})`

  return WithAuthenticationHoc;
};

export const withAuthenticationPropTypes = {
  authentication: PropTypes.shape({
    name: PropTypes.string,
    setName: PropTypes.func.isRequired,
  }).isRequired,
};
