import { Layout, Menu } from 'antd';
import React from 'react';
import injectSheet from 'react-jss';

import {
  withAuthentication,
  withAuthenticationPropTypes,
} from '../modules/Authentication';
import logo50 from '../images/logo50.png';
import { compose } from '../modules/util';
import { constants } from '../config';
import { SerialPortsList } from './SerialPortsList';

const { Header, Content, Footer } = Layout;

const styles = {
  img: {
    height: '50px',
    width: '50px',
    margin: '6px',
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  menu: {
    lineHeight: '64px',
  },
  content: {
    padding: '50px',
    backgroundColor: '#fff',
  }
};

function Home(props) {
  const { classes } = props;

  const onMenuClick = ({ key }) => {
    if (key === 'logout') {
      props.authentication.setName(undefined);
    }
  }

  return (
    <Layout>
      <Header className={classes.header}>
        <img src={logo50} className={classes.img} alt="logo" />
        <Menu
          theme="dark"
          mode="horizontal"
          className={classes.menu}
          onClick={onMenuClick}
        >
          <Menu.Item key="logout">{constants.LOG_OUT}</Menu.Item>
        </Menu>
      </Header>
      <Content className={classes.content}>
        <SerialPortsList />
      </Content>
      <Footer />
    </Layout>
  );
}

Home.proptypes = {
  ...withAuthenticationPropTypes,
}

export default compose(
  withAuthentication,
  injectSheet(styles),
)(Home);
