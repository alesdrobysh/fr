import React from 'react';
import ReactJson from 'react-json-view';

import { api, notification } from '../services';
import { constants } from '../config';

export class SerialPortsList extends React.Component {
  state = {
    list: [],
  };

  async componentDidMount() {
    try {
      const listResponse = await api.getSerialPortsList();
      this.setState({ list: listResponse.data })
    } catch (e) {
      console.error(e);
      notification.error(constants.FAILED_FETCHING_SERIAL_PORTS);
    }
  }

  render() {
    const { list } = this.state;

    return (
      <React.Fragment>
        <h1>{constants.SERIAL_PORTS}</h1>
        <ReactJson src={list} />
      </React.Fragment>
    )
  }
}
