import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Authorize from '../modules/Authentication/components/Authorize';
import Home from './Home';
import { AuthenticationProvider } from '../modules/Authentication'
import { ProtectedRoute } from '../modules/Authentication';

function App() {
  return (
    <AuthenticationProvider>
      <Router>
        <Switch>
          <ProtectedRoute path="/" exact component={Home} />
          <Route path="/authenticate/" component={Authorize} />
        </Switch>
      </Router>
    </AuthenticationProvider>
  );
};

export default App;
