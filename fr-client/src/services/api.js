import axios from 'axios';

export const recognize = (data /* formData */) => axios({
    data,
    method: 'post',
    url: '/recognize',
    config: { headers: { 'Content-Type': 'multipart/form-data' } },
});

export const register = (data /* { name, encoding } */) => axios({
    data,
    method: 'post',
    url: '/register',
});

export const getSerialPortsList = () => axios('/serialports');
