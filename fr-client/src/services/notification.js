import { message } from 'antd';

export const error = (text) => message.error(text);
export const success = (text) => message.success(text);