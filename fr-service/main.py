from flask import Flask, request
import json
from PIL import Image
import numpy as np
import dlib
from pkg_resources import resource_filename

# You can change this to any folder on your system
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}


def main():
    app = Flask(__name__)
    no_file_uploaded_response = 'No file uploaded'

    @app.route('/encode', methods=['POST'])
    def encode():
        # Check if a valid image file was uploaded
        # TODO: replace no_file_uploaded_response with HTTP error
        if 'file' not in request.files:
            return no_file_uploaded_response

        file = request.files['file']

        if file.filename == '':
            return no_file_uploaded_response

        if file and allowed_file(file.filename):
            # The image file seems valid! Detect faces and return the result.
            return detect_faces_in_image(file)

        # If no valid image file was uploaded, go to index:
        return no_file_uploaded_response

    @app.route('/recognize', methods=['POST'])
    def recognize():
        data = json.loads(request.get_json())

        storage = data['storage']

        if not storage:
            return None

        tolerance = data['tolerance']
        encoding = data['encoding']

        for item in storage:
            if (compare_faces(item['encoding'], encoding, tolerance)):
                return item['name']

        return 'json.dumps(ex)'

    app.run(host='0.0.0.0', port=5001, debug=True)


def detect_faces_in_image(file_stream):
    no_faces_recognized_response = 'No faces detected'

    # Load the uploaded image file
    img = np.array(Image.open(file_stream).convert('RGB'))

    try:
        # Get face encodings for any faces in the uploaded image
        result = recognize_face(img)

        return result
    except IndexError:
        return no_faces_recognized_response


def recognize_face(face_image):
    '''Face recognizer function'''
    # unknown_face = fr.face_encodings(face_image)[0]
    num_jitters = 1
    face_recognition_model = face_recognition_model_location()
    face_encoder = dlib.face_recognition_model_v1(face_recognition_model)
    face_rect = detect_face(face_image)

    if not face_rect:
        raise IndexError  # TODO Implement a custom exception type

    landmarks = detect_landmarks(face_image, face_rect)
    unknown_face = np.array(face_encoder.compute_face_descriptor(
        face_image, landmarks, num_jitters))

    return json.dumps(unknown_face.tolist())


def detect_landmarks(image, face_location):
    """
    Detects face landmarks im a passed image and returns a dlib rectangle object

    :param image: numpy array that represent an image
    :param face_location: dlib rectangle that represents the face location
    :return: dlib full_object_detection object that represents face landmarks
    """
    model = pose_predictor_model_location()
    predictor = dlib.shape_predictor(model)
    return predictor(image, face_location)


def detect_face(image):
    """
    Detects face im a passed image and returns a dlib rectangle object

    :param image: an image a face on it
    :return: a dlib 'rect' object
    """
    face_detector = dlib.get_frontal_face_detector()
    face_rects = face_detector(image)

    if len(face_rects) is 0:
        print("no face found")
        return np.empty((0))

    return face_rects[0]


def compare_faces(encoding1, encoding2, tolerance):
    if len(encoding1) is not len(encoding2):
        raise Exception('Encodings length are not compatible')
    distance = 0
    length = len(encoding1)

    for i in range(length):
        distance = distance + (encoding1[i] - encoding2[i]) ** 2

    return distance < tolerance


def face_recognition_model_location():
    return resource_filename(__name__, "models/dlib_face_recognition_resnet_model_v1.dat")


def pose_predictor_model_location():
    return resource_filename(__name__, "models/shape_predictor_68_face_landmarks.dat")


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


if __name__ == "__main__":
    main()
