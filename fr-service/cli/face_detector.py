from PIL import Image, ImageDraw
import numpy as np
import dlib


def open_image(file_path):
    im = Image.open(file_path)
    return np.array(im)


def detect_face(file_path):
    """
    Detects face im a passed image and returns a dlib rectangle object

    :param file_path: a path to a file to detect a face on it
    :return: a dlib 'rect' object
    """
    im = open_image(file_path)
    face_detector = dlib.get_frontal_face_detector()
    face_rects = face_detector(im)

    if len(face_rects) is 0:
        print("no face found")
        return

    return face_rects[0]


def plot_rect_to_image(filepath, filepath_clone, face_rect):
    """
    Plots a rectangle to a passed image and saves a copy

    :param filepath: a path to of source image
    :param filepath_clone: a path to save the image with rectangle
    :param face_rect: dlib rectangle
    """

    if not face_rect:
        print("no rectangle passed")
        return

    im = Image.open(filepath).copy()
    draw = ImageDraw.Draw(im)
    draw.rectangle([face_rect.left(), face_rect.top(),
                    face_rect.right(), face_rect.bottom()])
    im.save(filepath_clone)
