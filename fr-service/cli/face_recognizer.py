from PIL import Image, ImageDraw
import numpy as np
import dlib
from face_detector import detect_face
from pkg_resources import resource_filename


def face_recognition_model_location():
    return resource_filename(__name__, "models/dlib_face_recognition_resnet_model_v1.dat")


def pose_predictor_model_location():
    return resource_filename(__name__, "models/shape_predictor_68_face_landmarks.dat")


def detect_landmarks(image, face_location):
    """
    Detects face landmarks im a passed image and returns a dlib rectangle object

    :param image: numpy array that represent an image
    :param face_location: dlib rectangle that represents the face location
    :return: dlib full_object_detection object that represents face landmarks
    """
    model = pose_predictor_model_location()
    predictor = dlib.shape_predictor(model)

    return predictor(image, face_location)


def plot_landmarks_to_image(filepath, filepath_clone, landmarks):
    """
    Plots landmark points to a passed image and saves a copy

    :param filepath: a path to of source image
    :param filepath_clone: a path to save the image with rectangle
    :param face_rect: dlib full_object_detection object
    """

    if not landmarks:
        print("no landmarks passed")
        return

    im = Image.open(filepath).copy()
    draw = ImageDraw.Draw(im)
    for p in landmarks.parts():
        draw.ellipse((p.x - 5, p.y - 5, p.x + 5, p.y + 5))
    im.save(filepath_clone)


def face_encoding(face_image, num_jitters=1):
    """
    Given an image, return the 128-dimension face encoding for the detected face in the image.

    :param face_image: The image that contains one face
    :param known_face_locations: Optional - the bounding boxes of each face if you already know them.
    :param num_jitters: How many times to re-sample the face when calculating encoding. Higher is more accurate, but slower (i.e. 100 is 100x slower)
    :return: A list of 128-dimensional face encodings (one for each face in the image)
    """
    face_recognition_model = face_recognition_model_location()
    face_encoder = dlib.face_recognition_model_v1(face_recognition_model)

    landmarks = detect_landmarks(face_image, detect_face(face_image))

    return np.array(face_encoder.compute_face_descriptor(face_image, landmarks, num_jitters))
