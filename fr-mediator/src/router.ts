const express = require('express');
const multer = require('multer');

import { ApiResponse, Encoding, Face } from './core';
import { recognize, registerFace, encode } from './api';
import { listSerialPorts } from './serialport';

const router = express.Router();
const upload = multer();

/* formData */
router.post('/recognize', upload.any(), function (req, res) {
  /**
   * Proxies request to a recognizer
   */
  if (!req.files) {
    res.send({ message: ApiResponse.NoFileUploaded });
  }

  const file = req.files[0];

  if (!file) {
    res.send({ message: ApiResponse.NoFileUploaded });
  }

  if (!isFileAllowed(file.originalname)) {
    res.send({ message: ApiResponse.NoFileUploaded });
  }

  encode(file, async (error, proxyRes, body) => {
    if (error) {
      console.log(error);
      console.error(ApiResponse.NoFacesDetected);
      res.send({ message: ApiResponse.NoFacesDetected });

      return;
    }

    if (body === ApiResponse.NoFacesDetected) {
      console.error(ApiResponse.NoFacesDetected);
      res.send({ message: ApiResponse.NoFacesDetected });

      return;
    }

    try {
      const encoding: Encoding = JSON.parse(body);
      const name = await recognize(encoding);

      if (name) {
        res.send({
          name,
          message: ApiResponse.FaceRecognized,
        });
      } else {
        console.error(ApiResponse.FaceNotRecognized);
        res.send({
          encoding,
          message: ApiResponse.FaceNotRecognized,
        });
      }
    } catch (e) {
      console.log(body);
      console.error(e);
      res.status(500).send(e, body);
    }
  });
});

/* { name, encoding } */
router.post('/register', function (req, res) {
  const face: Face = req.body;
  if (face.name == null || !Array.isArray(face.encoding)) {
    res.status(400).send({ message: ApiResponse.InvalidFaceReceived });
    return;
  }
  registerFace(face);

  res.send({ message: ApiResponse.FaceRegistered });
});

router.get('/serialports', async function (req, res) {
  const ports = await listSerialPorts();
  res.send(ports);
})

function isFileAllowed(fileName) {
  const allowedExtensions = ['png', 'jpg', 'jpeg', 'gif'];
  return (
    fileName.includes('.')
    &&
    allowedExtensions.indexOf(fileName.split('.')[1].toLowerCase()) !== -1
  );
}

export default router;
