const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
require('dotenv').config();

import router from './router';

const PORT = 8081;
const HOST = '0.0.0.0';

const app = express();

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(router);

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
