import * as Mongoose from 'mongoose';
import { Face } from './core';

const {
  DB_USER,
  DB_PASS,
  DB_URL,
} = process.env;

Mongoose.connect(
  `mongodb://${DB_USER}:${DB_PASS}@${DB_URL}`,
  { useNewUrlParser: true },
  (err) => {
    if (err) {
      console.error('Error while connecting to the DB');
      console.error(err);
      return;
    }

    console.log('Connected to the DB');
  });

Mongoose.set('debug', (collectionName, method, query, doc) => {
  console.log(`${collectionName}.${method}`, JSON.stringify(query), doc);
});

const FaceSchema = {
  name: String,
  encoding: [Number],
}

export const FaceModel: Mongoose.Model<Face> = Mongoose.model('Face', FaceSchema);
