import { Face, Encoding, Name, FacesStorage, ComparisonResult } from './core';
import { FaceModel } from './db';
const request = require('request');
const requestPromise = require('request-promise-native');

const { RECOGNIZER_URL } = process.env;

export function registerFace(face: Face): Promise<void> {
  return FaceModel.create(face);
}

export function encode(file, callback) {
  console.log(`${RECOGNIZER_URL}/encode`);
  const options = {
    url: `${RECOGNIZER_URL}/encode`,
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/form-data',
      accept: 'application/json',
    },
  };

  /* body: encoding */
  const r = request.post(options, callback);

  const form = r.form();
  form.append('file', file.buffer,
    { contentType: 'image/png', filename: file.originalname });
}

export function compare(encoding: Encoding, storage: FacesStorage, tolerance: number) {
  const options = {
    url: `${RECOGNIZER_URL}/recognize`,
    method: 'POST',
    headers: {
      accept: 'application/json',
    },
    body: JSON.stringify({
      encoding,
      storage,
      tolerance,
    }),
    json: true,
  };

  return requestPromise.post(options);
}

export async function recognize(encoding: Encoding, tolerance: number = 0.1): Promise<Name | null> {
  try {
    const storage: FacesStorage = await FaceModel.find({}).exec();
    const storageLength = storage.length;

    if (storageLength === 0) {
      return null;
    }

    return compare(encoding, storage, tolerance);
  } catch (e) {
    console.error('ERROR', e);
    throw (e);
  }
}
