export type Name = string;
export type Encoding = number[];
export type Face = {
  name: Name;
  encoding: Encoding;
};
export type FacesStorage = Face[];

export enum ComparisonResult {
  Same = 'same',
  Different = 'different',
}

export enum ApiResponse {
  NoFileUploaded = 'No file uploaded',
  NoFacesDetected = 'No faces detected',
  FaceNotRecognized = 'Face is not recognized',
  FaceRecognized = 'Face is recognized',
  InvalidFaceReceived = 'Invalid face received',
  FaceRegistered = 'Face is registered',
}
