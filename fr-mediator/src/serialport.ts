const SerialPort = require('serialport');

export const listSerialPorts = () => new Promise((resolve, reject) => {
  SerialPort.list(function (err, ports) {
    if (err) {
      reject(err);
    }

    resolve(ports);
  });
});
