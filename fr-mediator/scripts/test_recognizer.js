require('dotenv').config();

const fs = require('fs');
const { compareFaces } = require('../lib/api');
const { FaceModel } = require('../lib/db');
const { ComparisonResult } = require('../lib/core');

const testFaces = JSON.parse(fs.readFileSync('test_encodings.json', 'utf8'));

console.log(`TEST DATA LENGTH: ${testFaces.length}`);

function testRecognizer(tolerance = 0.6) {
  return FaceModel.find({}).exec().then((storage) => {
    const storageLength = storage.length;

    if (storageLength === 0) {
      return null;
    }

    return testFaces.map((testFace) => {
      for (let i = 0; i < storageLength; ++i) {
        if (compareFaces(storage[i].encoding, testFace.encoding, tolerance) === ComparisonResult.Same) {
          console.log(`${testFace.name.slice(3, 6)} - ${storage[i].name.slice(0, 3)}`);
          return storage[i].name;
        }
      }
      console.log(`${testFace.name.slice(3, 6)} - null`);
      return null;
    });
  });
}

module.exports = testRecognizer;
