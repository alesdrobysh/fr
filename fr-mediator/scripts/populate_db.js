const dotenv = require('dotenv').config();

if (dotenv.error) {
  throw (dotenv.error);
}

const fs = require('fs');
const request = require('request-promise-native');

const { ApiResponse } = require('../lib/core');
const { registerFace } = require('../lib/api');

const { RECOGNIZER_URL } = dotenv.parsed;

function populateDb(folder) {
  if (!folder) {
    console.log('No folder with faces provided');
    return;
  }

  const files = fs.readdirSync(folder);
  const buffers = files.map(f => fs.readFileSync(`${folder}/${f}`));

  let options = {
    url: RECOGNIZER_URL,
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/form-data',
      accept: 'application/json',
    },
    pool: {
      maxSockets: 10000,
    },
  };

  files.map((file, fileIndex) => {
    const r = request.post(options);
    r.form().append('file', buffers[fileIndex],
      { contentType: 'image/png', filename: file });
    return r
      .catch((error) => {
        console.log(`${error.message} - ${file}`);
      })
      .then((body) => {
        if (!body) {
          return;
        }

        if (body === ApiResponse.NoFacesDetected) {
          console.error(ApiResponse.NoFacesDetected);
          return;
        }

        try {
          const encoding = JSON.parse(body);
          const name = file;
          registerFace({ name, encoding })
        } catch (e) {
          console.error('JSON parsing error ', body);
        }
      })
  });
}

module.exports = populateDb;
